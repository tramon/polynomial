﻿using System;

namespace TramonTask5_2 {

    class Applauncher {

        public static void Main(String[] args) {

            Polynomial p1 = new Polynomial(-1, 2);
            Polynomial p2 = new Polynomial(1, 3);
            Polynomial p3 = new Polynomial(1, -4);
            Polynomial p4 = new Polynomial(1, 0);
            Polynomial p5 = new Polynomial(1, 1);
            Polynomial p6 = new Polynomial(0, 1);
            Polynomial p7 = new Polynomial(0, 0);
            Polynomial p8 = p4 + p5;

            Polynomial additionResult = p1 + p2 + p3 + p4 + p5 + p7; 
            Polynomial subtractionResult = additionResult - p2;
            Polynomial multiplicationResult = p1 * p2 * p3;

            Console.WriteLine("Result of addition: \t" + additionResult);
            Console.WriteLine("Result of subtraction: \t" + subtractionResult);
            Console.WriteLine("Result of multiplying: \t" + multiplicationResult);
            Console.WriteLine("Example (1, 0): \t" + p4);
            Console.WriteLine("Example (1, 1): \t" + p5);
            Console.WriteLine("Example (0, 1): \t" + p6);
            Console.WriteLine("Example (0, 0): \t" + p7);
            Console.WriteLine("Example of addition:\t" + p8);

            Console.WriteLine("Example of addition:\t" + additionResult);
            additionResult[6] = -20;
            Console.WriteLine("Example of addition:\t" + additionResult);
            Console.ReadKey();
        }


    }




}
