﻿using System;

namespace TramonTask5_2 {

    // Polynomial with one fixed variable;
    // additional information is taken from here: http://www.mathsisfun.com/algebra/polynomials.html

    public class Polynomial {

        private const String VARIABLE = "x";
        private const String EXPONENT_SYMBOL = "^";
        private const String EMPTY_STRING = "";
        private const String PLUS_SEPARATOR = " + ";
        private const String MINUS_SEPARATOR = " - ";
        private const String MINUS = "-";

        private int[] constants;
        private int degree;

        public int this[int index] {
            get {
                return constants[index];
            }
            set {
                if (index >= 0 && index < constants.Length){
                    constants[index] = value;
                } else {
                    Console.Error.WriteLine("\nException: Please enter index in bounds of Polynomial!\n");
                    //throw new IndexOutOfRangeException();
                }
            }
        }

        public Polynomial(int constant, int exponent) {
            if (exponent < 0) {
                exponent *= -1;
            }
            constants = new int[1 + exponent];
            constants[exponent] = constant;
            degree = getDegree();
        }

        public int getDegree() {
            int resultDegree = 0;
            for (int i = 0; i < constants.Length; i++) {
                if (constants[i] != 0) {
                    resultDegree = i;
                }
            }
            return resultDegree;
        }

        public static Polynomial operator + (Polynomial leftOperand, Polynomial rightOperand) {
            int resultDegree = Math.Max(leftOperand.degree, rightOperand.degree);
            Polynomial resultingPolynomial = new Polynomial(0, resultDegree);

            for (int i = 0; i <= leftOperand.degree; i++) {
                resultingPolynomial.constants[i] += leftOperand.constants[i];
            }
            for (int i = 0; i <= rightOperand.degree; i++) {
                resultingPolynomial.constants[i] += rightOperand.constants[i];
            }
            resultingPolynomial.degree = resultingPolynomial.getDegree();
            return resultingPolynomial;
        }

        public static Polynomial operator - (Polynomial leftOperand, Polynomial rightOperand) {            
            int resultDegree = Math.Max(leftOperand.degree, rightOperand.degree);
            Polynomial resultingPolynomial = new Polynomial(0, resultDegree);

            for (int i = 0; i <= leftOperand.degree; i++) {
                resultingPolynomial.constants[i] += leftOperand.constants[i];
            }
            for (int i = 0; i <= rightOperand.degree; i++) {
                resultingPolynomial.constants[i] -= rightOperand.constants[i];
            }
            resultingPolynomial.degree = resultingPolynomial.getDegree();
            return resultingPolynomial;
        }

        public static Polynomial operator * (Polynomial leftOperand, Polynomial rightOperand) {
            int resultDegree = leftOperand.degree + rightOperand.degree;
            Polynomial resultingPolynomial = new Polynomial(0, resultDegree);

            for (int i = 0; i <= leftOperand.degree; i++) {
                for (int j = 0; j <= rightOperand.degree; j++) {
                    resultingPolynomial.constants[i + j] += (leftOperand.constants[i] * rightOperand.constants[j]);
                }
            }
            resultingPolynomial.degree = resultingPolynomial.getDegree();
            return resultingPolynomial;
        }

        public static Polynomial operator /(Polynomial leftOperand, Polynomial rightOperand) {
            Console.Error.WriteLine("\nException: Function is not implemented yet!\n");
            return null;
            //throw new NotSupportedException();
        }

        override public String ToString() {
            if (degree == 0) {
                return EMPTY_STRING + constants[0];
            }
            if (degree == 1) {
                if (constants [1] == 1) {
                    return VARIABLE;
                }
                return constants[1] + VARIABLE;
            } else if (degree == 1 && constants[0] > 0) {
                return constants[1] + VARIABLE + PLUS_SEPARATOR + constants[0];
            } else if (degree == 1 && constants[0] < 0) {
                return constants[1] + VARIABLE + MINUS_SEPARATOR + (-constants[0]);
            }

            String resultString = "";
            if (constants[degree] == 1) {
                resultString += VARIABLE + EXPONENT_SYMBOL + degree;
            } else if (constants[degree] == -1) {
                resultString += MINUS + VARIABLE + EXPONENT_SYMBOL + degree;
            } else { 
                resultString = constants[degree] + VARIABLE + EXPONENT_SYMBOL + degree;
            } 
            
            for (int i = degree - 1; i >= 0; i--) {

                if (constants[i] > 0) {

                    if ((constants[i] == -1 || constants[i] == 1) && i > 0) {
                        resultString += PLUS_SEPARATOR;
                    } else {
                        resultString += PLUS_SEPARATOR + (constants[i]);
                    }

                } else if (constants[i] < 0) {

                    if ((constants[i] == -1 || constants[i] == 1) && i > 0) {
                        resultString += MINUS_SEPARATOR;
                    } else {
                        resultString += MINUS_SEPARATOR + (-constants[i]);
                    }

                } else if (constants[i] == 0) {
                    continue;
                }

                if (i == 1) {
                    resultString = resultString + VARIABLE;
                } else if (i > 1) {
                    resultString = resultString + VARIABLE + EXPONENT_SYMBOL + i;
                } 
            }
            return resultString;
        }

    }
}
